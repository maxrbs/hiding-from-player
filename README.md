# Catch-up
## Description
### RU
Настройка ботов для игры в догонялки.
Боты бегут от игрока в безопасную локацию. 
Игрок должен прикоснуться к каждому из ботов и тем самым победить. 
### EN
Setting up bots for playing catch-up.
The bots run away from the player to a safe location.
The player must touch each of the bots to win.

## Screenshots
![Screen1](/Sharing/Screenshots/1.png)
![Screen2](/Sharing/Screenshots/2.png)
![Screen3](/Sharing/Screenshots/3.png)

## Links
Build on itch: https://maxrbs.itch.io/catch-up