﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    public Transform cameraFollowPoint;
    
    void Start()
    {
        transform.position = cameraFollowPoint.position;
        transform.LookAt(player.position);
    }
    
    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, cameraFollowPoint.position, 0.08f);
        transform.LookAt(player.position);
    }
}
