using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CatchArea : MonoBehaviour
{
    public UnityEvent OnCatchEvent;

    private void OnTriggerEnter(Collider other)
    {
        GameObject otherObject = other.gameObject;
        if (otherObject.tag == "Enemy")
        {
            otherObject.transform.parent = null;
            
            OnCatchEvent.Invoke();

            Destroy(otherObject);
        }

    }
}
