﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMoving : MonoBehaviour
{
    public Transform player;
    public Transform rayPointsContainer;

    public Vector3 fieldSize;

    private NavMeshAgent navMeshAgentComponent;
    private Vector3 targetShelterPosition;


    // Start is called before the first frame update
    void Start()
    {
        navMeshAgentComponent = GetComponent<NavMeshAgent>();
        player = FindObjectOfType<PlayerMoving>().gameObject.transform;

        targetShelterPosition = transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        Debug.DrawLine(player.position, targetShelterPosition);

        //если расстояние до укрытия меньше 2 метров (т.е. дошли до укрытия), проверяем, видит ли нас игрок
        //если больше, то проверяем укрытие 
        if (Vector3.Distance(transform.position, targetShelterPosition) < 2f)
        {
            if (CanPlayerSeeEnemy())
            {
                targetShelterPosition = GetNewShelterPosition();
                GoToShelterPosition();
            }
        }
        else
        {
            if (IsShelterReliable() == false)
            {
                targetShelterPosition = GetNewShelterPosition();
                GoToShelterPosition();
            }
        }
    }


    bool CanPlayerSeeEnemy()
    {
        if (Physics.Raycast(transform.position, player.position - transform.position, out RaycastHit hit))
            if (hit.collider.gameObject.tag == "Player")
                return true;

        return false;
    }
    
    Vector3 GetNewShelterPosition()
    {
        print("Start searching...");
        Quaternion oldRotation = transform.rotation;
        transform.LookAt(new Vector3(player.position.x, transform.position.y, player.position.z));
        transform.Rotate(0f, 180f, 0f);

        Vector3 newShelterPosition = player.position;

        // поиск укрытия впереди от игрока
        for (int i = 0; i < rayPointsContainer.childCount; i++)
        {
            if (IsPositionSafe(rayPointsContainer.GetChild(i).position))
            {
                //print("foundSafePos");
                newShelterPosition = rayPointsContainer.GetChild(i).position;
                break;
            }
        }

        // запуск поиска укрытия вокруг бота
        if (newShelterPosition == player.position)
            newShelterPosition = GetNewShelterAdvanced();

        // самоубийство
        if (newShelterPosition == player.position)
            newShelterPosition = transform.position;

        transform.rotation = oldRotation;

        //print("Stop searching...");
        return newShelterPosition;
    }

    Vector3 GetNewShelterAdvanced()
    {
        print("Start advanced searching...");

        Vector3 newShelterPosition = player.position;

        int iterationCount = 20;
        float rotateInterval = 360f / iterationCount;

        for (int i = 0; i < iterationCount; i++)
        {
            if (IsPositionSafe(rayPointsContainer.GetChild(12).position))
            {
                newShelterPosition = rayPointsContainer.GetChild(12).position;
                break;
            }
            
            transform.Rotate(0f, rotateInterval, 0f);
        }

        //print("Stop advanced searching...");
        return newShelterPosition;
    }

    void GoToShelterPosition()
    {
        navMeshAgentComponent.SetDestination(targetShelterPosition);
    }

    bool IsShelterReliable()
    {
        if (Physics.Raycast(targetShelterPosition, player.position - targetShelterPosition,  out RaycastHit hit))
            if (hit.collider.gameObject.tag == "Player")
                return false;

        return true;
    }

    bool IsPositionSafe(Vector3 pos)
    {
        if (Physics.Raycast(pos, player.position - pos, out RaycastHit hit))
        {
            // 1. Из этой позиции видно игрока
            if (hit.collider.gameObject.tag == "Player")
            {
                //print("unsafePos - playerCanSeeMeHere");
                return false;
            }

            Debug.DrawRay(new Vector3(pos.x, pos.y + 10f, pos.z), -Vector3.up * 12f, Color.cyan);

            // 2. Эта позиция занята другим объектом
            if (Physics.Raycast(new Vector3(pos.x, pos.y + 10f, pos.z), -Vector3.up, out RaycastHit hit2, 12f))
            {
               if (hit2.collider.gameObject.tag != "Ground")
                {
                    //print("unsafePos - canNotGetHere");
                    return false;
                }
            }

            // 3. Эта позиция находится за пределами карты
            if (Mathf.Abs(pos.x) > fieldSize.x/2f || Mathf.Abs(pos.z) > fieldSize.z/2f)
            {
                //print("unsafePos - outOfBounds");
                return false;
            }
        }

        return true;
    }
}
