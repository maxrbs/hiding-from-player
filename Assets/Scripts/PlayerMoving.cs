﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoving : MonoBehaviour
{
    public float movingSpeed = 5f;
    public float rotatingSpeed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W)) transform.Translate(Vector3.forward * Time.deltaTime * movingSpeed);
        else if (Input.GetKey(KeyCode.S)) transform.Translate(-Vector3.forward * Time.deltaTime * movingSpeed);

        if (Input.GetKey(KeyCode.D)) transform.Rotate(Vector3.up * Time.deltaTime * rotatingSpeed);
        else if (Input.GetKey(KeyCode.A)) transform.Rotate(Vector3.up * Time.deltaTime * -rotatingSpeed);

    }
}
