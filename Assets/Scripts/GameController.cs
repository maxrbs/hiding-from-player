using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Transform EnemiesContainer;

    public GameObject EndGameUI;
    public GameObject PauseUI;
    public Text CountToCatchText;

    private void Start()
    {
        UpdateCatchCountOnUI();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            TogglePauseMenu();

    }

    public void ReloadGame()
    {
        Scene currentScene = SceneManager.GetActiveScene();

        SceneManager.LoadScene(currentScene.name);
    }

    public void TryLoadEndGameUI()
    {
        if (EnemiesContainer.childCount == 0)
        {
            EndGameUI.SetActive(true);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void UpdateCatchCountOnUI()
    {
        CountToCatchText.text = "Left to catch: " + EnemiesContainer.childCount.ToString();
    }

    public void TogglePauseMenu()
    {
        if (EndGameUI.activeSelf)
            return;

        if (PauseUI.activeSelf)
        {
            PauseUI.SetActive(false);
            Time.timeScale = 1f;
        }
        else
        { 
            PauseUI.SetActive(true);
            Time.timeScale = 0f;
        }
        
    }
}
